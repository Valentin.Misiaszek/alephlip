import { Container, Heading } from '@chakra-ui/react'
import { Phlip } from '../components/Phlip'

const Home = () => {
  return (
    <Container textAlign={'center'}>
      <Heading size={'2xl'} px={9} mt={5} as="h1">
        Test your luck, Phlip the coin
      </Heading>
      <Phlip />
    </Container>
  )
}

export default Home
