'use client'

import React from 'react'
import { Header } from '../components/layout/Header'
import { ChakraProvider } from '@chakra-ui/react'
import { AlephlipProvider } from '@/contexts/AlephlipProvider'
import { QueryClient, QueryClientProvider } from 'react-query'

const queryClient = new QueryClient()

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en">
      <body>
        <ChakraProvider>
          <QueryClientProvider client={queryClient}>
            <AlephlipProvider>
              <Header />
              {children}
            </AlephlipProvider>
          </QueryClientProvider>
        </ChakraProvider>
      </body>
    </html>
  )
}
