'use client'
import React, { useState } from 'react'
import {
  Box,
  Button,
  Flex,
  FormControl,
  FormHelperText,
  FormLabel,
  HStack,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  Text
} from '@chakra-ui/react'
import { useAlephlip } from '@/hooks/useAlephlip'
import { useWallet } from '@alephium/web3-react'

interface PhlipProps {}

export const Phlip: React.FC<PhlipProps> = () => {
  const [userChoice, setUserChoice] = useState<'heads' | 'tails' | undefined>(undefined)
  const [errorMessage, setErrorMessage] = useState<string | undefined>()
  const [amountValue, setAmountValue] = useState<number>(0)

  const { fields, callPlay } = useAlephlip()
  const toto = useWallet()
  console.log(toto)

  const chooseHeads = () => {
    setErrorMessage('')
    setUserChoice('heads')
  }

  const chooseTails = () => {
    setErrorMessage('')
    setUserChoice('tails')
  }

  const handleValueChange = (value: number) => {
    setErrorMessage('')
    setAmountValue(value)
  }

  const validateForm = () => {
    if (userChoice === undefined) {
      setErrorMessage('Please choose heads or tails')
      return false
    }

    if (amountValue > 100 || amountValue <= 0) {
      setErrorMessage('Please enter a valid amount')
      return false
    }

    if (!userChoice) return false

    return true
  }

  const resetForm = () => {
    setUserChoice(undefined)
    setAmountValue(0)
    setErrorMessage('')
  }

  const validateBet = async () => {
    if (!validateForm()) return
    console.log('play')
    await callPlay(userChoice!, amountValue)
    resetForm()
  }

  return (
    <Flex direction={'column'} alignItems={'center'} mt={10}>
      <Box background={'blue.500'} boxSize={'100px'} borderRadius={'100%'}></Box>
      <FormControl mt={5} w={'40%'}>
        <FormLabel textAlign={'center'}>Bet amount</FormLabel>
        <NumberInput
          onChange={(_, valueAsNumber) => handleValueChange(valueAsNumber)}
          value={amountValue}
          max={100}
          min={0.01}
          precision={2}
          step={1}
        >
          <NumberInputField />
          <NumberInputStepper>
            <NumberIncrementStepper />
            <NumberDecrementStepper />
          </NumberInputStepper>
        </NumberInput>
        <FormHelperText>Max: 100 ALPH</FormHelperText>
      </FormControl>
      <HStack mt={5} gap={12}>
        <Button
          onClick={chooseHeads}
          w={'80px'}
          colorScheme={'blue'}
          variant={userChoice === 'heads' ? 'solid' : 'outline'}
        >
          Heads
        </Button>
        <Button
          onClick={chooseTails}
          w={'80px'}
          colorScheme={'red'}
          variant={userChoice === 'tails' ? 'solid' : 'outline'}
        >
          Tails
        </Button>
      </HStack>
      <Text mt={2} color={'red'} visibility={!!errorMessage ? 'visible' : 'hidden'}>
        {errorMessage}
      </Text>
      <Button onClick={validateBet} size={'lg'} colorScheme={userChoice === 'heads' ? 'blue' : 'red'} mt={4}>
        Phlip
      </Button>
      {fields && (
        <Text color={'grey'} textDecoration={'underline'} mt={10}>
          {Number(fields.feesPercentage)}% fees
        </Text>
      )}
    </Flex>
  )
}
