'use client'
import {
  Button,
  Flex,
  Heading,
  HStack,
  IconButton,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverTrigger,
  Text,
  Tooltip,
  useColorMode
} from '@chakra-ui/react'
import { Moon, Sun, Wallet2 } from 'lucide-react'
import React from 'react'
import { AlephiumConnectButtonCustom } from '@alephium/web3-react'

export const Header = () => {
  return (
    <Flex as="header" h="10vh" w="100%" alignItems="center" justifyContent="space-between" px={5}>
      <Flex alignItems={'baseline'}>
        <Heading mr={1} as={'h1'}>
          Alephlip
        </Heading>
        <Heading color={'yellow.500'} fontSize={'3em'}>
          .
        </Heading>
      </Flex>
      <UserControlPanel />
    </Flex>
  )
}

function UserControlPanel() {
  return (
    <HStack gap={2}>
      <ThemeModeSwitch />
      <CustomAlephiumConnectButton />
    </HStack>
  )
}

function ThemeModeSwitch() {
  const { colorMode, toggleColorMode } = useColorMode()

  return (
    <Tooltip label={colorMode === 'dark' ? 'Light mode' : 'Dark mode'}>
      <IconButton onClick={toggleColorMode} aria-label="light-mode" icon={colorMode === 'dark' ? <Sun /> : <Moon />} />
    </Tooltip>
  )
}

function CustomAlephiumConnectButton() {
  return (
    <AlephiumConnectButtonCustom>
      {({ isConnected, disconnect, show, account }) => {
        return isConnected ? (
          <Popover>
            <PopoverTrigger>
              <Button w={'120px'}>
                <Text textOverflow={'ellipsis'} overflow={'hidden'} whiteSpace={'nowrap'}>
                  {account!.address}
                </Text>
              </Button>
            </PopoverTrigger>
            <PopoverContent>
              <PopoverArrow />
              <PopoverCloseButton />
              <PopoverBody>
                <Flex w={'100%'} justifyContent={'center'}>
                  <Button colorScheme={'red'} onClick={disconnect}>
                    Disconnect
                  </Button>
                </Flex>
              </PopoverBody>
            </PopoverContent>
          </Popover>
        ) : (
          <Tooltip label={'Connect Wallet'}>
            <IconButton aria-label={'connect-wallet'} icon={<Wallet2 />} onClick={show} />
          </Tooltip>
        )
      }}
    </AlephiumConnectButtonCustom>
  )
}
