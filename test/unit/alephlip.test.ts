import { Alephlip, AlephlipTypes } from '../../artifacts/ts'
import { beforeAll, describe, expect, it } from 'vitest'
import { addressFromContractId, Project, TestContractParams, web3 } from '@alephium/web3'
import { randomContractId, testAddress } from '@alephium/web3-test'

describe('unit tests', () => {
  let testContractId: string
  let testContractAddress: string
  let testParamsFixture: TestContractParams<AlephlipTypes.Fields, { amount: bigint }>

  beforeAll(async () => {
    web3.setCurrentNodeProvider('http://127.0.0.1:22973', undefined, fetch)
    await Project.build()
    testContractId = randomContractId()
    testContractAddress = addressFromContractId(testContractId)
    testParamsFixture = {
      // a random address that the test contract resides in the tests
      address: testContractAddress,
      // assets owned by the test contract before a test
      initialAsset: { alphAmount: 1n * 10n ** 18n },
      // initial state of the test contract
      initialFields: {
        feesPercentage: 3n,
        poolOwner: testAddress,
        feesBalance: 0n,
        poolBalance: 0n
      },
      testArgs: { amount: 1n },
      // assets owned by the caller of the function
      inputAssets: [{ address: testAddress, asset: { alphAmount: 1n * 10n ** 18n } }]
    }
  })

  // Get fees balance
  it('should return fees balance', async () => {
    const params = {
      ...testParamsFixture,
      initialFields: { ...testParamsFixture.initialFields, feesBalance: 1n * 10n ** 18n }
    }
    const testResult = await Alephlip.tests.getFeesBalance(params)
    expect(testResult.returns).toEqual(1n * 10n ** 18n)
  })

  // Owner deposit
  it('should deposit in the contract pool balance', async () => {
    const testResult = await Alephlip.tests.ownerDeposit(testParamsFixture)
    const contractState = testResult.contracts[0] as AlephlipTypes.State
    expect(contractState.address).toEqual(testContractAddress)
    expect(contractState.fields.poolBalance).toEqual(1n)
  })

  it('should return code error 3 if depositor is not the owner', async () => {
    const testParams = {
      ...testParamsFixture,
      inputAssets: [
        { address: '1DrDyTr9RpRsQnDnXo2YRiPzPW4ooHX5LLoqXrqfMrp55', asset: { alphAmount: 1n * 10n ** 18n } }
      ]
    }

    try {
      await Alephlip.tests.ownerDeposit(testParams)
    } catch (error: any) {
      expect(error.toString()).toMatch(/VM execution error: AssertionFailedWithErrorCode\(\w+,3\)/)
    }
  })

  // Withdraw fees
  it('should withdraw fees', async () => {
    let params = {
      ...testParamsFixture,
      initialFields: { ...testParamsFixture.initialFields, feesBalance: 1n * 10n ** 15n },
      testArgs: {}
    }
    const testResult = await Alephlip.tests.withdrawFees(params)
    const contractState = testResult.contracts[0] as AlephlipTypes.State

    expect(contractState.address).toEqual(testContractAddress)
    expect(contractState.fields.feesBalance).toEqual(0n)
  })
})
