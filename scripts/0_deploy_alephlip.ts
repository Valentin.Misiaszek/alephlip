import { Deployer, DeployFunction, Network } from '@alephium/cli'
import { Settings } from '../alephium.config'
import { Alephlip } from '../artifacts/ts'

const deployAlephlip: DeployFunction<Settings> = async (
  deployer: Deployer,
  network: Network<Settings>
): Promise<void> => {
  const initialPoolFeesPercentage = network.settings.initialPoolFeesPercentage
  const initalDevFeesPercentage = network.settings.initalDevFeesPercentage
  const result = await deployer.deployContract(Alephlip, {
    initialFields: {
      poolFeesPercentage: BigInt(initialPoolFeesPercentage as number),
      devFeesPercentage: BigInt(initalDevFeesPercentage as number),
      poolOwner: deployer.account.address,
      devFeesBalance: 0n,
      poolBalance: 0n
    }
  })

  console.log('Alephlip contract id: ' + result.contractInstance.contractId)
  console.log('Alephlip contract address: ' + result.contractInstance.address)
}

export default deployAlephlip
