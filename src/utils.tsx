import { NetworkId } from '@alephium/web3'
import { loadDeployments } from '../artifacts/ts/deployments'

export interface AlephlipConfig {
  network: NetworkId
  groupIndex: number
  alephlipAddress: string
}

function getNetwork(): NetworkId {
  const network = (process.env.NEXT_PUBLIC_NETWORK ?? 'devnet') as NetworkId
  return network
}

function getAlephlipConfig(): AlephlipConfig {
  const network = getNetwork()
  const alephlip = loadDeployments(network).contracts.Alephlip.contractInstance
  const groupIndex = alephlip.groupIndex
  const alephlipAddress = alephlip.address
  return { network, groupIndex, alephlipAddress }
}

export const alephlipConfig = getAlephlipConfig()
