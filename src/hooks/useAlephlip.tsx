'use client'
import { NodeProvider, web3 } from '@alephium/web3'
import { Alephlip, AlephlipInstance } from '../../artifacts/ts'
import { useEffect, useState } from 'react'
import { useToast } from '@chakra-ui/react'

interface AlephlipFields {
  feesPercentage: bigint
  poolBalance: bigint
  feesBalance: bigint
  poolOwner: string
}

export const useAlephlip = () => {
  const toast = useToast()

  const [alephlip, setAlephlip] = useState<AlephlipInstance>()
  const [fields, setFields] = useState<AlephlipFields>({
    feesPercentage: 0n,
    poolBalance: 0n,
    feesBalance: 0n,
    poolOwner: ''
  })

  const fetchContractState = async () => {
    try {
      if (alephlip) {
        const state = await alephlip.fetchState()
        setFields(state.fields as AlephlipFields)
      }
    } catch (error) {
      console.error(error)
      toast({
        title: 'Error',
        description: 'An error occurred while fetching contract state',
        status: 'error',
        duration: 9000,
        isClosable: true
      })
    }
  }

  const callPlay = async (choice: 'heads' | 'tails', amount: number) => {
    if (alephlip) {
      try {
        const { returns: result } = await alephlip.methods.play({
          args: {
            isHeads: choice === 'heads',
            amount: BigInt(amount)
          }
        })

        return result
      } catch (error) {
        console.error(error)
        toast({
          title: 'Error',
          description: 'An error occurred while playing',
          status: 'error',
          duration: 9000,
          isClosable: true
        })
      }
    }
  }

  useEffect(() => {
    const nodeUrl = 'http://127.0.0.1:22973'
    const nodeProvider = new NodeProvider(nodeUrl)
    web3.setCurrentNodeProvider(nodeProvider)

    const alephlipAdress = 'yHTha1KrCw9H8tHMr2csSenUjEN2Q8TfeVaX1A2tBYv3'
    const alephlip = Alephlip.at(alephlipAdress)
    setAlephlip(alephlip)
  }, [])

  useEffect(() => {
    fetchContractState()
  }, [alephlip])

  return { alephlip, fields, callPlay }
}
