'use client'
import React from 'react'
import { AlephiumWalletProvider } from '@alephium/web3-react'
import { alephlipConfig } from '@/utils'

interface AlephlipProviderProps {
  children: React.ReactNode
}

export const AlephlipProvider: React.FC<AlephlipProviderProps> = ({ children }) => {
  return (
    <AlephiumWalletProvider theme="midnight" network={alephlipConfig.network} addressGroup={alephlipConfig.groupIndex}>
      {children}
    </AlephiumWalletProvider>
  )
}
